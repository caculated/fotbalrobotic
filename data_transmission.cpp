/*

  Aici punem toate functiile care au legatura cu primirea/transmiterea datelor serverului.

*/

#include <stdio.h>
#include <iostream>
#include <mosquitto.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "structures.h"
#include "declarations.h"
#include "functions.h"
using namespace std;

void connect_callback(struct mosquitto *mosq, void *obj, int result)
{
return;

}
void request_publish(int up_pub)
{
    int mid; 
    char q[4];
    char p[5] = "/r";
    sprintf(q, "%d",up_pub);     
    strcat(p,q); 
    if(DEBUG == 1)
    {
    cout <<   "control["<<  up_pub << "].left=" << control[up_pub].left << "\n";
    cout <<   "control["<<  up_pub << "].right=" << control[up_pub].right << "\n";
    cout <<   "control[" << up_pub << "].left_rotation=" << control[up_pub].left_rotation <<"\n";
    cout <<   "control[" << up_pub << "].right_rotation=" << control[up_pub].right_rotation << "\n";
    }
    if(DEBUG_SLOW == 1)
     sleep(1);   

    mosquitto_publish(mosq, &mid, p, sizeof(struct RobotControl), &control[up_pub], 0, false);
   
}
void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    struct RobotCoords *coordonate = (struct RobotCoords *)message->payload;
    if (coords[coordonate->id].timestamp<=coordonate->timestamp&&coordonate->x!=0&&coordonate->y!=0)
    {
        //coords[coordonate->id].id = coordonate->id;
        past3[coordonate->id].x = past2[coordonate->id].x;
        past3[coordonate->id].y = past2[coordonate->id].y;
        past3[coordonate->id].angle = past2[coordonate->id].angle;
        past3[coordonate->id].timestamp = past2[coordonate->id].timestamp;
        past3[coordonate->id].timestampusec = past2[coordonate->id].timestampusec;

        past2[coordonate->id].x = past[coordonate->id].x;
        past2[coordonate->id].y = past[coordonate->id].y;
        past2[coordonate->id].angle = past[coordonate->id].angle;
        past2[coordonate->id].timestamp = past[coordonate->id].timestamp;
        past2[coordonate->id].timestampusec = past[coordonate->id].timestampusec;

        past[coordonate->id].x = coords[coordonate->id].x;
        past[coordonate->id].y = coords[coordonate->id].y;
        past[coordonate->id].angle = coords[coordonate->id].angle;
        past[coordonate->id].timestamp = coords[coordonate->id].timestamp;
        past[coordonate->id].timestampusec = coords[coordonate->id].timestampusec;

        coords[coordonate->id].x  = coordonate->x;
        coords[coordonate->id].y  = coordonate->y;
        coords[coordonate->id].angle = coordonate->angle;
        coords[coordonate->id].timestamp = coordonate->timestamp;
        coords[coordonate->id].timestampusec = coordonate->timestampusec;
       // if (coordonate->id==13||coordonate->id==13) cerr<<"Idob: "<<coordonate->id<<"Coords("<<coordonate->x<<":"<<coordonate->y<<")"<<"Angle:"<<coordonate->angle<<endl; //in loc de cout pentru a afisa in consola ca pe eroare

    }
    if (coordonate->id==0) cerr<<"("<<coordonate->x<<":"<<coordonate->y<<")"<<coordonate->angle<<endl;

    if (DEBUG==1)
    {
        if (past2[0].x==past[0].x&&past[0].x==coords[0].x&&past2[0].y==past[0].y&&past[0].y==coords[0].y) cerr<<"nu se misca"<<endl;
    }
}
int need_to_send()
{

    struct timeval now;

    gettimeofday(&now, NULL);

    if(tv.tv_usec > 800000)
        gettimeofday(&tv, NULL);

    if(now.tv_usec >= tv.tv_usec + 200000) {
            return 1;
    }

    return 0;
}
void do_robot_control_loop()
{
    int mid;
    fflush(stdout);

    play_on();

    request_publish(RAMBO);
    request_publish(EL_DIABLO);
    request_publish(PUTIN);
    request_publish(TYSON);
    request_publish(HERCULES);

    gettimeofday(&tv, NULL);
}
