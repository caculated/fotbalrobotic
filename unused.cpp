/*
int x_rel (int x0,int y0,int x,int y,int a) /// relativ la 0,robot
{
    return (int) (x*cos(a*PI/180)+y*sin(a*PI/180)-x0*cos(a*PI/180)-y0*sin(a*PI/180));
}
int y_rel (int x0,int y0,int x,int y,int a) /// relativ la 0,robot
{
    return (int) -1*(-x*sin(a*PI/180)+y*cos(a*PI/180)+x0*sin(a*PI/180)-y0*cos(a*PI/180));
}




//Misca robotul in fata ('r') sau in spate ('b') cu o putere si numar de rotatii date.
void basic_move(int robot_id, char direction, int power, int nr_of_rotations)
{
    switch(direction)
    {
    case 'f':
        control[robot_id].left=power;
        control[robot_id].right=power;
        control[robot_id].time=0;
        control[robot_id].left_rotation=nr_of_rotations;
        control[robot_id].right_rotation=nr_of_rotations;
        break;

    case 'b':
        control[robot_id].left=-power;
        control[robot_id].right=-power;
        control[robot_id].time=0;
        control[robot_id].left_rotation=nr_of_rotations;
        control[robot_id].right_rotation=nr_of_rotations;
        break;

    default:
        std::cerr<<"'switch'' statement from 'basic_move' did not receive direction. Did not execute.";
        break;
    }

}

//Intoarce robotul la stanga ('l') sau la dreapta ('r') la o putere data.
void basic_turn(int robot_id, char direction, int power, int nr_of_rotations)
{
    switch(direction)
    {
    case 'l': //case "left".
        control[robot_id].left=0;
        control[robot_id].right=power;
        control[robot_id].time=0;
        control[robot_id].left_rotation=0;
        control[robot_id].right_rotation=nr_of_rotations;
        break;

    case 'r': //case "right".
        control[robot_id].left=power;
        control[robot_id].right=0;
        control[robot_id].time=0;
        control[robot_id].left_rotation=nr_of_rotations;
        control[robot_id].right_rotation=0;
        break;

    default:
        std::cerr<<"'switch' statement from 'basic_turn' did not receive direction. Did not execute.";
        break;
    }
}
//Misca robotu pe un arc de cerc
void robot_sense(double unghi_obiectiv,int robot_id)
{
    double unghi_relativ = coords[robot_id].angle - unghi_obiectiv;
    unghi_relativ *= semn(unghi_relativ);
    if (unghi_relativ>PI/2||unghi_relativ<(PI*3)/2) sens[robot_id] = PI;
    else sens[robot_id] = 0.0;
}
int dreapta (int x1,int y1,int x2,int y2)   /// nu uita,modul
{
    return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}

const struct point bstgj = {-40,290};
const struct point bdrpj = {-40,420};
const struct point bstgs = {1435,290};
const struct point bdrps = {1435,420};
struct point pr;
int err = 10;
int c = 0;

bool blockstgj()
{

  if((bstgj.x - err <= pr.x && pr.x <= bstgj.x + err) && (bstgj.y - err <= pr.y &&  pr.y <= bstgj.y + err))
      return 1;
  return 0;
}

bool blockdrpj()
{
    /*
  cout <<"Este la" << "(" << pr.x << ":" << pr.y << ")" << "\n";
  cout <<  pr.x  << " " <<  bdrpj.x  << "\n";
  cout << (bdrpj.x - err < pr.x && pr.x > bdrpj.x + err) << "\n";
    /
  if((bdrpj.x - err <= pr.x && pr.x <= bdrpj.x + err) && (bdrpj.y - err <= pr.y &&  pr.y  <= bdrpj.y + err))
      return 1;
  return 0;
}
bool blockdrps()
{

  if((bdrps.x - err <= pr.x && pr.x <= bdrps.x + err) && (bdrps.y - err <= pr.y &&  pr.y <= bdrps.y + err ))
      return 1;
  return 0;
}

bool blockstgs()
{

  if((bstgs.x - err <= pr.x && pr.x <= bstgs.x + err) && (bstgs.y - err <= pr.y &&  pr.y  <= bstgs.y + err))
      return 1;
  return 0;
}

void restgj() // Terenul de jos partea stanga
{

}
void redrpj()// Terenul de jos partea dreapta
{

}
void restgs()// Terenul de sus partea stanga
{


}
void redrps()// Tereneul de sus partea stanga
{

}
void deblock(int id)
{

    if(id == -1)
      restgj();
    if(id == -2)
      redrpj();
    if(id == 1)
      redrps();
    if(id == 2)
      redrpj();
}
int block()
{
  if(blockstgj())
      return -1;
  if(blockdrpj())
      return -2;
  if(blockdrps())
      return 1;
  if(blockdrpj())
      return 2;
  return 0;
}
void go(bool a)
{
   int q=1;
   q = (a)? q : -q;
   control[portar].left = control[portar].right = 70*q;
   control[portar].left_rotation = control[portar].right_rotation = 12;

}
void *start(void *p)
{
    int x;
    pr.x = coords[portar].x;
    pr.y = coords[portar].y;
    int angle = coords[portar].angle;
    bool sw;
    while( 1 )
    {
      if(need_to_send())
      {
        //if(1)
       //     return 0;
       x = block();
       if(x != 0)
       {
           control[portar].left = 45;
           control[portar].right = 45;

           deblock(x);

       }

       //do my staff

       cout << "PORTAR: " << block() << "\n";
       request_publish(portar);
      }
      angle= coords[portar].angle;
      pr.x = coords[portar].x;
      pr.y = coords[portar].y;

    }

}
void init_portar()
{
   //go_to(rP, poarta.x, poarta.y);
   // cout << "Aa";
    int *p;
    t1.idt = pthread_create(&(t1.thread), NULL, start, (void *) p);

}


void rotire_spre_minge_si_pornire_spre_minge ()
{
    control[rId].right = 30;
    control[rId].left = -30;
    control[rId].time = 0;
    control[rId].right_rotation = 32000;
    control[rId].left_rotation  = 32000;

    request_publish(rId);

    ///while(atan2(coords[rId].y - coords[0].y,coords[rId].x - coords[0].x)-10>coords[rId].angle  &&  coords[rId].angle < atan2(coords[rId].y - coords[0].y,coords[rId].x - coords[0].x)+10);
    while(70 < coords[rId].angle && coords[rId].angle > 110);


    control[rId].right = 60;
    control[rId].left = 60;
    control[rId].time = 0;
    control[rId].right_rotation = 32000;
    control[rId].left_rotation  = 32000;

    request_publish(rId);
}

*/
