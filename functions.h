#include "structures.h"

/*

 Ii spunem calculatorului ce functii exista. Fiecare fisier care contine 'functions.h' (este inclus), va stii
 ca functiile de jos exista undeva.

*/

void play_on(); //functia de baza. Calculeaza tot ce fac robotii.

//TESTING
void verify();
void wherewillitbe(double t);
void rotire_spre_minge_si_pornire_spre_minge();

//BASIC_MOVEMENT
void basic_move(int robot_id, char direction, int power, int nr_of_rotations);
void basic_turn(int robot_id, char direction, int power, int nr_of_rotations);
void mergi_in_fata(int id_robot, int viteza);
void roteste_simplu(int id_robot,int s,int s2);
void roteste_o_roata(int id_robot,int s,int s2);
int semn(int x);
int semnd(double x);
void robot_sense(double unghi_obiectiv,int robot_id);
int unghiuldorit2 (int i);
void rotate(int id, int z);
int x_rel (int x0,int y0,int x,int y,int a);
int y_rel (int x0,int y0,int x,int y,int a);
int dreapta (int x1,int y1,int x2,int y2);
int angle (int x1,int y1,int x2,int y2);
void rotatie_cu_o_roata (int id,int a,int s,int s_d,bool ready);
void miscare_liniara (int id,int d,int s,int y,int a,bool ready);
void path_planing(int id,int x,int y,int a,bool &b,int v[100]);
void path_execution(int id,int x,int y,int a,bool &b,int v[100]);
void dupa_minge(int id);
int modul(int x);
int get_correct(int x);

//NEW
void follow_ball(int id);
void rotate_new (int id,int angle);
void linear_movement (int id,int x,int y);
void calculate_next_position(int id,int x_prime[17][3],int timp,int y_prime[17][3],int time_prime[17][3]);
inline void say_coords();

//DATA_TRANSMISSION
void connect_callback(struct mosquitto *mosq, void *obj, int result);
void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);
int need_to_send();
void do_robot_control_loop();
void *get_connect( void *p);
void *get_subscribe(void *p);
void *get_publish(void *p);
void request_publish(int id);
double incadrarea_unghiului(double alpha);

int unghiuldorit (int id, int x, int y);
int predictball(int i);
void init_portar();

//ATACANT
void ataca_mingea(int id_robot);
void roteste_simplu(int id_robot);
void mergi_in_fata(int id_robot, int viteza);
void ataca_mingea2(int id_robot);
void take_coordinates(int robot_id);
void position( int robo, double x, double y );
void strategie_pentru_crimeea (int id);

void escape(int robo);
bool same_position(int robo);
void ataca(int robo);

bool care_poarta(int id);
void halt_movement(int robo);
bool reset_traiectorie();

void retragere(int id_robot,int id_robot2);
void go_to(int id_robot ,point p);

//FUNDASI
bool activate_PUTIN();
bool activate_TYSON();
bool russia_attacked();
bool usa_attacked();
bool desactivate_PUTIN();
bool desactivate_TYSON();
void return_TYSON();
void return_PUTIN();
void move_to_brut(int robo, int x, int y);

void rezerva();
void reglare();
void stopminge();

bool interval(int x,int a,int b);
bool aproximare();
bool bregl();

//NEW AGE
void portar_fct(int id);
RobotCoords points_on_the_poarta(RobotCoords x, RobotCoords y, int robo);
RobotCoords point_on_the_line(RobotCoords x, RobotCoords y, int d1);
int test_if_within_circle(RobotCoords a,RobotCoords b,int radius);
RobotCoords calculate_target(RobotCoords target,int id,int radius);
int manta_sau_nu(int id, int radius);
void attack(RobotCoords thisR,RobotCoords poarta,RobotCoords target,int id);
void attack_cu_spatele(RobotCoords thisR,RobotCoords poarta,RobotCoords target,int id);

void go_to_pozitie_cu_spatele2(RobotCoords thisR,RobotCoords target,RobotCoords pozitie,int id);
void pasivedef(int id);


void activedef (int id);
