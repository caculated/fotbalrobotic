#include "structures.h"
#include <sys/time.h>
#include <mosquitto.h>

/*

 Aici definim variabilele, adica alocam efectiv memorie pentru ele. Asta, daca nu aloci in alta parte.
 De exemplu la control[] si coords[].
 In acest fisier se definesc DOAR si TOATE VARIABILELE GLOBALE.

*/

RobotControl control[17];
RobotCoords  coords[17], past[17], past2[17], past3[17];
timeval  tv;
mosquitto *mosq;

//LES ROBOTS
int RAMBO = 3;
int EL_DIABLO = 5;
int HERCULES = 8;
int PUTIN = 15;
int TYSON = 2;

char robotId[] = "/r14";
unsigned int rId = 14;

bool ok[24];
qthread t1, t2, t3;
char clientid[24]="Cal"; //de ce 24?
double unghi_aproximativ_0,unghi_foarte_mic,unghi_mic,unghi_aproape_0,unghi_mediu,unghi_mare,unghi_foarte_mare; ///constante
double sens[17];
const short int VITEZA100 = 100;
bool up_coords = 0;
int up_pub = -1;
int rP;
int mingX,mingY;
bool ming;
bool b=0;
int v[100];


point m;

//ATACANT
bool gata_de_atac = 0;
double md=0, unghi_md=0;
int timp_temporizator = 0;
int gata_temporizator = 10;
int start_temporizator = 0;


bool sau1sau0=1;
int semne[17]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int semne2[17]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
unsigned k=0,k1[20],k2[20],k3=0;

int level[17] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int last_x[17] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, last_y[17] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, last_angle[17] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int btime[17] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};

//FUNDASI
int zona1_x1 = 0, zona1_x2 = 0, zona2_x1 = 0, zona2_x2 = 0;
int zona1_y1 = 0, zona1_y2 = 0, zona2_y1 = 0, zona2_y2 = 0;
point mij_ter = {700,350};
point zona1_1, zona1_2, zona2_1, zona2_2;
point pTYSON, pPUTIN;
int forward_activated = 0;
int cooltime = 0;

//NEW AGE
int josx=28,
    susx=1368,
    stangay=283,
    dreaptay=417;

int tinta_stangay=stangay, tinta_dreaptay=dreaptay, tinta_x=susx, defense_dreapta=515, defense_stanga=184, defense_x=214,
atac_x=abs(tinta_x-defense_x), aparare_x=abs(abs(1400-tinta_x)-defense_x), linie_atac=(tinta_x+atac_x)/2, linie_aparare=(abs(1400-tinta_x)+aparare_x)/2;

int danger=39, danger_universal=1359, extreme_danger=-26, extreme_danger_universal=1429;//A SE MODIFICA IN FUNCTIE DE PARTEA TERENULUI
int left_side=-4, right_side=705, upper_side=1341, down_side=48;//A SE CALIBRA DACA NU CORESPUND (se ia fix pozitia robotului)
int poarta_jos_x=49, poarta_sus_x=1322, poarta_stanga_y=275, poarta_dreapta_y=449, poarta_jos_predictie=33, poarta_sus_predictie=1370,
poarta_stanga_predictie=279,poarta_dreapta_predictie=425;

int o1=8, o2=3;
char id_o1[] = "/r8", id_o2[]="/r3";
int d1=16, d2=7;
char id_d1[]="/r16", id_d2[]="/r7";

int p=11;

char id_p[]="/r11";
int e1=3, e2=7, e3=2, e4=3, e5=7;
int latimer=48, diametrur=66;
int razab=15, diametrub=30;
int distantax=0, distantay=0;

int target_x=atac_x, target_y=(tinta_dreaptay+tinta_stangay)/2;
int timp=0, timp2=0, k1_1=0, k2_2=0, eroare=1, reset=0, reset_sus=1385, reset_jos=18;
int po1x=536, po1y=423, po2x=531, po2y=282, pd1x=238, pd1y=520, pd2x=238, pd2y=190, ppx=80, ppy=350;
int of1, of2, def1, def2, port;
