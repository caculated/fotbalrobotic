#include "structures.h"
#include "functions.h"
#include "declarations.h"
#include <iostream>
#include <unistd.h>
using namespace std;

bool interval(int x,int a,int b)
{
    return (a<=x && x<=b);
}
bool bregl()
{
    if (B)
    {
        return (coords[0].x>700);
    }

    else
    {
        return (coords[0].x<700);
    }
}
bool care_poarta(int id)
{
    int a = unghiuldorit(id,coords[0].x,coords[0].y);
    if (B)
    {
        return (a<=90||270<=a);
    }
    else
    {
        return (90<=a&&a<=270);
    }
}
bool same_position(int robo)
{
    int laminus,laplus;
    int angle=coords[robo].angle;
    int xprim=coords[robo].x;
    int yprim=coords[robo].y;
    if(last_x[robo]==-1&&xprim!=0) last_x[robo]=xprim;
    if(last_y[robo]==-1&&yprim!=0) last_y[robo]=yprim;
    if(last_angle[robo]==-1&&angle!=0)
    {
        last_angle[robo]=angle;
        laplus=last_angle[robo]+3;
        laplus-=360;
        laminus=last_angle[robo]-3;
        if (laminus<0) laminus+=360;

    }

    if(btime[robo]%6==0)
    {
        if(last_x[robo]!=-1&&last_y[robo]!=-1&&last_angle[robo]!=-1&&xprim<=last_x[robo]+10&&xprim>=last_x[robo]-10&&yprim<=last_y[robo]+10&&yprim>=last_y[robo]-10)
                        if(laminus<laplus)
                                if(angle<=laplus&&angle>=laminus)return 1;
                        else if (angle<=laplus||angle>=laminus)return 1;
        last_x[robo] = -1; last_y[robo] = -1; last_angle[robo] = -1;
    }

    btime[robo]++;
    btime[robo]%=6;
    return 0;
}
bool reset_traiectorie()
{
    int a=unghiuldorit2(56);
    int a2=unghiuldorit2(1);
    int a_plus=a+30;
    a_plus%=360;
    int a_minus=a-30;
    if (a_minus<0)
    {
        a_minus=-a_minus;
        a_minus%=360;
        a_minus=360-a_minus;
    }
    if (a_plus>a_minus) return !(a_plus<a2&&a_minus>a2);
    else return !(a_plus<a2||a_minus>a2);
}
bool aproximare()
{
    double x,y;
    x=past[0].x-coords[0].x;
    y=past[0].y-coords[0].y;
    double a=atan2(y,x);
    a=a*180/PI;
    if (a<0) a=-a;
    if (a>70&&a<110) return 1;
    return 0;
}
