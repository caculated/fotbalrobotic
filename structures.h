#ifndef STRUCTURES_H
#define STRUCTURES_H
#include <pthread.h>
struct RobotControl{
    int left;
    int right;
    int time;
    int left_rotation;
    int right_rotation;
};
struct qthread
{
    pthread_t thread;
    int idt;
};
struct RobotCoords {
    int id; // id robot - 1..10
    int x;  // 0...800  - relative la terenul de joc
    int y;  // 0...600
    int angle; // unghi fata de baza ringului
    int timestamp; // timpul cand au fost calculate - UNIXTIME - Ex: 1352460922
    int timestampusec;
};
struct point{
int x,y;
};

#endif




