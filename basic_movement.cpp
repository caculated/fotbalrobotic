#include "declarations.h"
#include "functions.h"
#include <cmath>
#include <iostream>
#include <unistd.h>
using namespace std;
int sw = 1;

void escape(int robo)
{
    switch(level[robo])
    {
        case 1:
            control[robo].left = 150;
            control[robo].right = -150;
            control[robo].left_rotation = 320;
            control[robo].right_rotation = 320;
            control[robo].time = 0;
            level[robo]++;
            break;

       case 2:
            control[robo].left = 150;
            control[robo].right = 150;
            control[robo].left_rotation = 20;
            control[robo].right_rotation = 20;
            control[robo].time = 0;
            level[robo]++;
            break;

       case 3:
            control[robo].left = -150;
            control[robo].right = -150;
            control[robo].left_rotation = 20;
            control[robo].right_rotation = 20;
            control[robo].time = 0;
            level[robo] = 1;
            break;
    }
}
void halt_movement(int robo)
{
    control[robo].left=0;
    control[robo].right=0;
    control[robo].left_rotation=0;
    control[robo].right_rotation=0;
    control[robo].time=0;
}
void mergi_in_fata(int id_robot, int viteza)
{
    control[id_robot].left = viteza;
    control[id_robot].right =viteza;
    control[id_robot].left_rotation = 12;
    control[id_robot].right_rotation = 12;
}
void roteste_simplu(int id_robot,int s,int s2)//NOTE: o functie de rotatie, ca sa nu se miste pe un arc de cerc, trebuie sa miste ambele motoare.
{
    control[id_robot].left = -40*s*s2;
    control[id_robot].right = 40*s*s2;
    control[id_robot].left_rotation = 1;
    control[id_robot].right_rotation = 1;
}
void roteste_o_roata(int id_robot,int s,int s2)
{
    if(s==1)
    {
    control[id_robot].left =  70*s2;
    control[id_robot].right = 0;
    control[id_robot].left_rotation = 2;
    control[id_robot].right_rotation = 0;
    }
    else
    {
    control[id_robot].left = 0;
    control[id_robot].right = 70*s2;
    control[id_robot].left_rotation = 0;
    control[id_robot].right_rotation = 2;
    }
}
void inpoarta()
{
    int sinus,cosinus;
    int yminge=predictball(B);
    if (coords[portar].angle<90&&coords[portar].angle>270) cosinus=-1;
    else cosinus=1;
    if(coords[portar].angle>180) sinus=-1;
    else sinus=1;
    if(( 1300<=coords[portar].x && coords[portar].x <= 1400)|| (0<=coords[portar].x && coords[portar].x<=100))
    {
    if (coords[portar].angle>80&&coords[portar].angle<100)
    {
        int semn3=1;
        if (yminge>coords[portar].y)semn3=-1;
        control[portar].left= 55*semn3;
        control[portar].right=55*semn3;
        control[portar].left_rotation= 1+modul(yminge-coords[portar].y)/60;
        control[portar].right_rotation=1+modul(yminge-coords[portar].y)/60;
        cout<<"1"<<endl;
    }
    else if (coords[portar].angle>260&&coords[portar].angle<280)
    {

        int semn3=-1;
        if (yminge>coords[portar].y)semn3=1;
        control[portar].left= -55*semn3;
        control[portar].right=-55*semn3;
        control[portar].left_rotation= 1+modul(yminge-coords[portar].y)/60;
        control[portar].right_rotation=1+modul(yminge-coords[portar].y)/60;
        cout<<"2"<<endl;
    }
    else
    {
        control[portar].left= -40 * sinus * cosinus;
        control[portar].right=40 * sinus *cosinus;
        control[portar].left_rotation= 1;
        control[portar].right_rotation=1;
        cout<<"3"<<endl;
    }
  }
    else
    {
        if(B)
        {
            int semn1;
            if (coords[portar].x>30)semn1=-1;
            else semn1=1;
            control[portar].left=-40*cosinus*semn1;
            control[portar].right=-40*cosinus*semn1;
            control[portar].left_rotation= 1;
            control[portar].right_rotation=1;
            cout<<"4aici noob"<<endl;
        }
        else
        {
            int semn1;
            if (coords[portar].x>1370)semn1=-1;
            else semn1=1;
            control[portar].left=-40*cosinus*semn1;
            control[portar].right=-40*cosinus*semn1;
            control[portar].left_rotation= 1;
            control[portar].right_rotation=1;
            cout<<"5ba aici"<<endl;
        }
    }
    if(yminge+5>coords[portar].y&&yminge-5>coords[portar].y)
    {
        control[portar].left=0;
        control[portar].right=0;
        control[portar].left_rotation= 0;
        control[portar].right_rotation=0;
        cout<<"6ai pierdut"<<endl;
    }

}
void inpoarta2()
{
    int sinus,cosinus;
    int yminge=predictball(B);
    cout<<"yminge="<<yminge<<endl;
    if (coords[portar].angle<90&&coords[portar].angle>270) cosinus=-1;
    else cosinus=1;
    if(coords[portar].angle>180) sinus=-1;
    else sinus=1;
    if(( 1300<=coords[portar].x && coords[portar].x <= 1400)||(0<=coords[portar].x && coords[portar].x<=100))
    {
        cout<<"1"<<endl;
        if(coords[portar].angle>75&&coords[portar].angle<105||coords[portar].angle>255&&coords[portar].angle<285)
        {
            cout<<"2"<<endl;
            int semn13;
            if(yminge-coords[portar].y<0)semn13=-1;
            else semn13=1;
            if(yminge<coords[portar].y-5&&yminge>coords[portar].y+5)
            {
                cout<<"3"<<endl;

                control[portar].left= 0;
                control[portar].right=0;
                control[portar].left_rotation= 0;
                control[portar].right_rotation=0;
            }
            else if(yminge>coords[portar].y-5||yminge<coords[portar].y+5)
            {
                cout<<"4"<<endl;

                control[portar].left= -65*semn13*sinus;
                control[portar].right=-65*semn13*sinus;
                control[portar].left_rotation= 3;
                control[portar].right_rotation=3;
            }
        }
        else
        {

            control[portar].left= -40*cosinus;
            control[portar].right= 40*cosinus;
            control[portar].left_rotation= 1;
            control[portar].right_rotation=1;

        }

    }
}
void stopminge()
{
    int sinus,cosinus;
    int s;
    int ang=coords[portar].angle;
    int ybleh=predictball(B);
    if (coords[portar].angle>180) sinus=-1;
    else sinus=1;
    if(ybleh-coords[portar].y<0)s=-1;
    else s=1;
    if(!(interval(ang,20,160) || interval(ang,200,340)))
    {
        cout<<"dap"<<endl;
        if (ang>180) sinus=-1;
        else sinus=1;
        if (ang>90&&ang<270)cosinus=-1;
        else cosinus=1;

        control[portar].left= -40*sinus*cosinus;
        control[portar].right=40*sinus*cosinus;
        control[portar].left_rotation= 1;
        control[portar].right_rotation=1;
    }
    else if(s*(ybleh-coords[portar].y)>20)
    {
        if(k1[portar]>0)k1[portar]--;
        control[portar].left= -(50+k1[portar]*15)*sinus*s;
        control[portar].right=-(50+k1[portar]*15)*sinus*s;
        control[portar].left_rotation= 2;
        control[portar].right_rotation=2;
    }
    else k1[portar]=4;

}
void reglare()
{
    int cosinus,sinus;
    int id_robot=portar;
    int xminus=1320,xplus=1400;
    int ang = coords[portar].angle;
    point tP; tP.x = coords[portar].x; tP.y = coords[portar].y;
    if (B)
    {
        xminus=0;
        xplus=80;
    }
    cout <<k3<<endl;
    if((tP.x<xminus||tP.x>xplus))
    {
        cout<<"a"<<endl;
        cout<<ang<<endl;
        if (ang>90&&ang<270)cosinus=-1;
        else cosinus=1;
        int s;
        if(B)
        {
            if (tP.x>50) s=-1;
            else s=1;
        }
        else
        {
            if (tP.x>1350) s=-1;
            else s=1;
        }
        control[portar].left= 90*cosinus*s;
        control[portar].right=90*cosinus*s;
        control[portar].left_rotation= 2;
        control[portar].right_rotation=2;


    }
    else if(!(interval(ang,75,105) || interval(ang,245,285)))
    {

        cout<<"b"<<endl;
        if (ang>180) sinus=-1;
        else sinus=1;
        if (ang>90&&ang<270)cosinus=-1;
        else cosinus=1;

        control[portar].left= -40*sinus*cosinus;
        control[portar].right=40*sinus*cosinus;
        control[portar].left_rotation= 1;
        control[portar].right_rotation=1;
    }
    else if ((tP.y<285||tP.y>415))
    {
        cout<<"tP.y"<<tP.y<<endl;
        cout<<"c"<<endl;
        cout<<"ang="<<ang<<endl;
        sinus=1;
        int s=1;
        if (ang>180)sinus=-1;
        if (tP.y<350) s=-1;
        control[portar].left= 80*sinus*s;
        control[portar].right=80*sinus*s;
        control[portar].left_rotation= 4;
        control[portar].right_rotation=4;
        cout<<"s"<<s<<endl;
        cout<<"sinus"<<sinus<<endl;
    }
    else
    {
        cout<<"d"<<endl;
        control[portar].left= 0;
        control[portar].right=0;
        control[portar].left_rotation= 0;
        control[portar].right_rotation=0;
    }
}
void rezerva()
{
    control[portar].left= 150;
    control[portar].right= 50;
    control[portar].left_rotation= 320;
    control[portar].right_rotation=320;
}
