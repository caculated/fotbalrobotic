#include "structures.h"
#include <cmath>
#include <math.h>
#include <sys/time.h>
#include <mosquitto.h>
using namespace std;

/*

 Declaram variabilele globale, prin "extern". A declara = a-i spune calculatorului ca ele exista.
 Acesta e headerul in care punem DOAR si TOATE VARIABILELE GLOBALE.
 Se leaga de "structures.h" pentru ca avem nevoie de structurile definite acolo.

*/
#define DEBUG 0
#define DEBUG_SLOW 0
#define gpi2 7
#define mqtt_host "192.168.0.100"  //adresa ip a serverului
#define mqtt_port 1883
#define PI 3.14159265
#define B 1 // B-poarta atacata,0 poarta de la x 0, 1 poarta de la x 1400 invers
#define portar 1
#define C 0 //0 - tactica R, 1 - tactica Vali cel Frumos.

//LES ROBOTS
extern int RAMBO, EL_DIABLO, PUTIN, HERCULES, TYSON;

extern point m;
extern RobotControl control[17];
extern RobotCoords  coords[17], past[17], past2[17], past3[17];
extern char robotId[];
extern unsigned int rId;
extern timeval tv;
extern mosquitto *mosq;
extern double unghi_aproape_0,unghi_foarte_mic,unghi_mic,unghi_mediu,unghi_mare,unghi_foarte_mare; ///constante
extern qthread t1,t2,t3;
extern char clientid[24];
extern const short int VITEZA100;
extern point poarta;
extern bool  up_coords;
extern double sens[17];
extern int up_pub;
extern bool b;
extern int v[100];
extern int rP;
extern int mingX,mingY;
extern bool ming;

extern bool sau1sau0;
extern int semne[17];
extern int semne2[17];
extern unsigned k,k1[20],k2[20],k3;

//ATACANT
extern bool gata_de_atac;
extern double md, unghi_md;
extern int gata_temporizator;
extern int timp_temporizator;
extern int start_temporizator;
extern int btime[], last_x[], last_y[], last_angle[], level[];

//FUNDASI
extern int zona1_x1, zona1_x2, zona2_x1, zona2_x2;
extern point zona1_1, zona1_2, zona2_1, zona2_2;
extern int zona1_y1, zona1_y2, zona2_y1, zona2_y2;
extern point mij_ter;
extern point pTYSON, pPUTIN;
extern int forward_activated;
extern int cooltime;

//NEW AGE
extern int josx, susx, stangay, dreaptay;
extern int tinta_stangay, tinta_dreaptay,tinta_x, defense_dreapta, defense_stanga, defense_x, atac_x, aparare_x, linie_atac, linie_aparare;
extern int danger, danger_universal, extreme_danger, extreme_danger_universal;//A SE MODIFICA IN FUNCTIE DE PARTEA TERENULUI
extern int left_side, right_side, upper_side, down_side;//A SE CALIBRA DACA NU CORESPUND (se ia fix pozitia robotului)
extern int poarta_jos_x, poarta_sus_x, poarta_stanga_y, poarta_dreapta_y,
poarta_jos_predictie, poarta_sus_predictie, poarta_stanga_predictie, poarta_dreapta_predictie;

extern int o1, o2;
extern char id_o1[], id_o2[];
extern int d1, d2;
extern char id_d1[], id_d2[];

extern int p;

extern char id_p[];
extern int e1,e2,e3,e4,e5;
extern int latimer,diametrur;
extern int razab,diametrub;
extern int distantax,distantay;

extern int target_x;
extern int target_y;

extern int timp,timp2;
extern int k1_1,k2_2;
extern int eroare;

extern int reset,reset_sus,reset_jos;

extern int po1x,po1y,po2x,po2y,pd1x,pd1y,pd2x,pd2y,ppx,ppy;
extern int of1,of2,def1,def2,port;

