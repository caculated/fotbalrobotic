/*

 In play_on va sta toata strategia noastra spre victorie.

*/

#include "structures.h"
#include "functions.h"
#include "declarations.h"
#include <iostream>
#include <unistd.h>
using namespace std;
bool s=1;

void play_on()
{
    //HERCULES
    portar_fct(HERCULES);

    //RAMBO
    if (same_position(RAMBO))escape(RAMBO);
    else ataca(RAMBO);

    //EL_DIABLO
    if (same_position(EL_DIABLO))escape(EL_DIABLO);
    else ataca(EL_DIABLO);

    //TYSON
    if (same_position(TYSON))escape(TYSON);
    else pasivedef(TYSON);

    //PUTIN
    if (same_position(PUTIN))escape(PUTIN);
    else activedef (PUTIN);

}
