#include "functions.h"
#include "declarations.h"
#include "structures.h"
#include <iostream>
using namespace std;

bool activate_PUTIN()
{
    int mx = coords[0].x;
    if(mx >= zona1_x1 && mx <= zona1_x2)return 1;
    return 0;
}
bool activate_TYSON()
{
    int mx = coords[0].x;
    if(mx >= zona2_x1 && mx <= zona2_x2)return 1;
    return 0;
}
void go_to(int id_robot ,point p)
{
    double x1,y1;
    x1 = p.x;
    y1 = p.y;
    double robot_unghi = coords[id_robot].angle;
    int s,s2;
    int v;
    int x=coords[id_robot].x,y=coords[id_robot].y;

        int unghi_atac = unghiuldorit(id_robot,x1,y1);

        int ar=unghi_atac-coords[id_robot].angle;
        if (ar<0) ar += 360;

        if (k%3)
        {
            if(ar>180) semne[id_robot]=-1;
            else semne[id_robot]=1;
            if(ar>90&&ar<270) semne2[id_robot]=-1;
            else semne2[id_robot]=1;
        }
        s = semne[id_robot];
        s2= semne2[id_robot];

        if (s2==-1)unghi_atac+=180;


        int limita_plus = unghi_atac + 15;
        if (limita_plus>=360) limita_plus%=360;
        int limita_minus = unghi_atac - 15;
        if (limita_minus<0)
        {
            limita_minus=-limita_minus;
            limita_minus%=360;
            limita_minus=360-limita_minus;
        }


        if (limita_plus>limita_minus)
        {
            if(robot_unghi > limita_plus || robot_unghi < limita_minus)
            {
                if(k1[id_robot]==0)
                {
                    int limita_plus2 = unghi_atac + 30;
                    if (limita_plus2>=360) limita_plus2%=360;
                    int limita_minus2 = unghi_atac - 30;
                    if (limita_minus2<0)
                    {
                        limita_minus2=-limita_minus2;
                        limita_minus2%=360;
                        limita_minus2=360-limita_minus2;
                    }
                    int limita_plus3 = unghi_atac + 60;
                    if (limita_plus3>=360) limita_plus3%=360;
                    int limita_minus3 = unghi_atac - 60;
                    if (limita_minus3<0)
                    {
                        limita_minus3=-limita_minus3;
                        limita_minus3%=360;
                        limita_minus3=360-limita_minus3;
                    }


                    control[id_robot].left = -40*s*s2;
                    control[id_robot].right = 40*s*s2;
                    if (robot_unghi > limita_plus3 || robot_unghi < limita_minus3)
                    {
                        control[id_robot].left_rotation = 3;
                        control[id_robot].right_rotation = 3;
                    }
                    else if(robot_unghi > limita_plus2 || robot_unghi < limita_minus2)
                    {
                        control[id_robot].left_rotation = 2;
                        control[id_robot].right_rotation = 2;
                    }
                    else
                    {
                        control[id_robot].left_rotation = 1;
                        control[id_robot].right_rotation = 1;
                    }

                }




                k1[id_robot]++;
                k2[id_robot]=0;
                if (k1[id_robot]>=8)k1[id_robot]=0;
            }
            else
            {
                cout<<"in else"<<endl;
                if (k2[id_robot]<11) k2[id_robot]++;
                v=k2[id_robot]*10;
                control[id_robot].left = (50+v)*s2;
                control[id_robot].right = (50+v)*s2;
                control[id_robot].left_rotation = 16;
                control[id_robot].right_rotation = 16;
                k1[id_robot]=0;

            }

        }
        else
        {
            if(robot_unghi > limita_plus && robot_unghi < limita_minus)
            {
                if(k1[id_robot]==0)
                {
                    int limita_plus2 = unghi_atac + 30;
                    if (limita_plus2>=360) limita_plus2%=360;
                    int limita_minus2 = unghi_atac - 30;
                    if (limita_minus2<0)
                    {
                        limita_minus2=-limita_minus2;
                        limita_minus2%=360;
                        limita_minus2=360-limita_minus2;
                    }
                    int limita_plus3 = unghi_atac + 60;
                    if (limita_plus3>=360) limita_plus3%=360;
                    int limita_minus3 = unghi_atac - 60;
                    if (limita_minus3<0)
                    {
                        limita_minus3=-limita_minus3;
                        limita_minus3%=360;
                        limita_minus3=360-limita_minus3;
                    }


                    control[id_robot].left = -40*s*s2;
                    control[id_robot].right = 40*s*s2;
                    if (robot_unghi > limita_plus3 && robot_unghi < limita_minus3)
                    {
                        control[id_robot].left_rotation = 3;
                        control[id_robot].right_rotation = 3;
                    }
                    else if(robot_unghi > limita_plus2 && robot_unghi < limita_minus2)
                    {
                        control[id_robot].left_rotation = 2;
                        control[id_robot].right_rotation = 2;
                    }
                    else
                    {
                        control[id_robot].left_rotation = 1;
                        control[id_robot].right_rotation = 1;
                    }

                }




                k1[id_robot]++;
                k2[id_robot]=0;
                if (k1[id_robot]>=8)k1[id_robot]=0;
            }
            else
            {
                cout<<"in else"<<endl;
                if (k2[id_robot]<11) k2[id_robot]++;
                v=k2[id_robot]*10;
                control[id_robot].left = (50+v)*s2;
                control[id_robot].right = (50+v)*s2;
                control[id_robot].left_rotation = 16;
                control[id_robot].right_rotation = 16;
                k1[id_robot]=0;

            }

        }



        //cerr<<(robot_unghi <= limita_plus && robot_unghi >= limita_minus)<<endl;

}
void return_PUTIN()
{
    int unghi_PUTIN = coords[PUTIN].angle;
    if(B==0)
    {
        if((unghi_PUTIN >= 320 && unghi_PUTIN <=360)||(unghi_PUTIN <= 30 && unghi_PUTIN >=0))
        {
            control[PUTIN].left = 80;
            control[PUTIN].right = 80;
            control[PUTIN].left_rotation = 10;
            control[PUTIN].right_rotation = 10;
            control[PUTIN].time = 0;
        }
        else
        {
            control[PUTIN].left = -40;
            control[PUTIN].right = 40;
            control[PUTIN].left_rotation = 2;
            control[PUTIN].right_rotation = 2;
            control[PUTIN].time = 0;
        }
    }
    else
    {
        if(unghi_PUTIN >= 135 && unghi_PUTIN <= 220)
        {
            control[PUTIN].left = 80;
            control[PUTIN].right = 80;
            control[PUTIN].left_rotation = 10;
            control[PUTIN].right_rotation = 10;
            control[PUTIN].time = 0;
        }
        else
        {
            control[PUTIN].left = -40;
            control[PUTIN].right = 40;
            control[PUTIN].left_rotation = 2;
            control[PUTIN].right_rotation = 2;
            control[PUTIN].time = 0;
        }
    }
}
void return_TYSON()
{
    int unghi_TYSON = coords[TYSON].angle;
    if(B==0)
    {
        if((unghi_TYSON >= 320 && unghi_TYSON <=360)||(unghi_TYSON <= 30 && unghi_TYSON >=0))
        {
            control[TYSON].left = 80;
            control[TYSON].right = 80;
            control[TYSON].left_rotation = 10;
            control[TYSON].right_rotation = 10;
            control[TYSON].time = 0;
        }
        else
        {
            control[TYSON].left = -40;
            control[TYSON].right = 40;
            control[TYSON].left_rotation = 2;
            control[TYSON].right_rotation = 2;
            control[TYSON].time = 0;
        }
    }
    else
    {
        if(unghi_TYSON >= 135 && unghi_TYSON <= 225)
        {
            control[TYSON].left = 80;
            control[TYSON].right = 80;
            control[TYSON].left_rotation = 10;
            control[TYSON].right_rotation = 10;
            control[TYSON].time = 0;
        }
        else
        {
            control[TYSON].left = -40;
            control[TYSON].right = 40;
            control[TYSON].left_rotation = 2;
            control[TYSON].right_rotation = 2;
            control[TYSON].time = 0;
        }
    }
}
void basic_rotate(int robo)
{
    control[robo].left = -30;
    control[robo].right = 30;
    control[robo].left_rotation = 2;
    control[robo].right_rotation = 2;
    control[robo].time = 0;
}
void forward(int robo)
{
    forward_activated = 1;
    control[robo].left = 100;
    control[robo].right = 100;
    control[robo].left_rotation = 16;
    control[robo].right_rotation = 16;
    control[robo].time = 0;
}
void move_to_brut(int robo, int x, int y)
{
    int ur = coords[robo].angle;
    int part = unghiuldorit(robo, x, y)/15 + 1;

    if(forward_activated == 0)
    switch(part)
    {
    case 1:
        if(ur >= 0 && ur <= 15)forward(robo);
        else basic_rotate(robo);
        break;

    case 2:
        if(ur >= 15 && ur <= 30)forward(robo);
        else basic_rotate(robo);
        break;

    case 3:
        if(ur >= 30 && ur <= 45)forward(robo);
        else basic_rotate(robo);
        break;

    case 4:
        if(ur >= 45 && ur <= 60)forward(robo);
        else basic_rotate(robo);
        break;

    case 5:
        if(ur >= 60 && ur <= 75)forward(robo);
        else basic_rotate(robo);
        break;

    case 6:
        if(ur >= 75 && ur <= 90)forward(robo);
        else basic_rotate(robo);
        break;

    case 7:
        if(ur >= 90 && ur <= 105)forward(robo);
        else basic_rotate(robo);
        break;

    case 8:
        if(ur >= 105 && ur <= 120)forward(robo);
        else basic_rotate(robo);
        break;

    case 9:
        if(ur >= 120 && ur <= 135)forward(robo);
        else basic_rotate(robo);
        break;

    case 10:
        if(ur >= 135 && ur <= 150)forward(robo);
        else basic_rotate(robo);
        break;

    case 11:
        if(ur >= 150 && ur <= 165)forward(robo);
        else basic_rotate(robo);
        break;

    case 12:
        if(ur >= 165 && ur <= 180)forward(robo);
        else basic_rotate(robo);
        break;

    case 13:
        if(ur >= 180 && ur <= 195)forward(robo);
        else basic_rotate(robo);
        break;

    case 14:
        if(ur >= 195 && ur <= 210)forward(robo);
        else basic_rotate(robo);
        break;

    case 15:
        if(ur >= 210 && ur <= 225)forward(robo);
        else basic_rotate(robo);
        break;

    case 16:
        if(ur >= 225 && ur <= 240)forward(robo);
        else basic_rotate(robo);
        break;

    case 17:
        if(ur >= 240 && ur <= 255)forward(robo);
        else basic_rotate(robo);
        break;

    case 18:
        if(ur >= 255 && ur <= 270)forward(robo);
        else basic_rotate(robo);
        break;

    case 19:
        if(ur >= 270 && ur <= 285)forward(robo);
        else basic_rotate(robo);
        break;

    case 20:
        if(ur >= 285 && ur <= 300)forward(robo);
        else basic_rotate(robo);
        break;

    case 21:
        if(ur >= 300 && ur <= 315)forward(robo);
        else basic_rotate(robo);
        break;

    case 22:
        if(ur >= 315 && ur <= 330)forward(robo);
        else basic_rotate(robo);
        break;

    case 23:
        if(ur >= 330 && ur <= 345)forward(robo);
        else basic_rotate(robo);
        break;

    case 24:
        if(ur >= 345 && ur <= 360)forward(robo);
        else basic_rotate(robo);
        break;
    }
    else if(cooltime%2 == 0) forward_activated = 0;
    else forward(robo);
}



void pasivedef(int id)
{
    int x=coords[0].x;
    int y=coords[0].y;
    RobotCoords target,pozitie;
    RobotCoords thisR=coords[id];
    if (B)
    {
        if (x<700)
        {
            if (x<100)
            {
                if(y<225)
                {
                    if(thisR.x>=150)
                    {
                        target.y=pozitie.y=225;
                        target.x=-100;
                        pozitie.x=90;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=pozitie.y=225;
                        target.x=-100;
                        pozitie.x=20;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else if (y>375)
                {
                    if(thisR.x>=150)
                    {
                        target.y=pozitie.y=375;
                        target.x=-100;
                        pozitie.x=90;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=pozitie.y=375;
                        target.x=-100;
                        pozitie.x=20;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else
                {
                    target.y=700;
                    target.x=pozitie.x=250;
                    pozitie.y=350;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(y>600)
            {
                target.y=700;
                target.x=pozitie.x=x-100;
                pozitie.y=680;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else if(y<100)
            {
                target.y=0;
                target.x=pozitie.x=x-100;
                pozitie.y=20;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else if ((x-thisR.x)*(x-thisR.x)+(y-thisR.y)*(y-thisR.y)<=250)
            {
                int se=1;
                if (y<thisR.y)se=-1;
                control[id].left=-150*se;
                control[id].right=150*se;
                control[id].left_rotation=32;
                control[id].right_rotation=32;
            }
            else if (x*x+(y-350)*(y-350)>22500)
            {
                target.y=0;
                target.x=pozitie.x=x/2;
                pozitie.y=350+(y-350)/2;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else
            {
                target.y=700;
                target.x=pozitie.x=250;
                pozitie.y=350;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }

        }
        else
        {
            target.y=700;
            target.x=pozitie.x=250;
            pozitie.y=350;
            go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
        }

    }
    else
    {
        if(x>700)
        {
            if (x>1300)
            {
                if(y<225)
                {
                    if(thisR.x<=1250)
                    {
                        target.y=pozitie.y=225;
                        target.x=1500;
                        pozitie.x=1310;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=pozitie.y=225;
                        target.x=1500;
                        pozitie.x=1380;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else if (y>375)
                {
                    if(thisR.x<=1250)
                    {
                        target.y=pozitie.y=375;
                        target.x=1500;
                        pozitie.x=1310;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=pozitie.y=375;
                        target.x=1500;
                        pozitie.x=1380;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else
                {
                    target.y=700;
                    target.x=pozitie.x=1150;
                    pozitie.y=350;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(y>600)
            {
                target.y=700;
                target.x=pozitie.x=x+100;
                pozitie.y=680;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else if(y<100)
            {
                target.y=0;
                target.x=pozitie.x=x+100;
                pozitie.y=20;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else if ((x-thisR.x)*(x-thisR.x)+(y-thisR.y)*(y-thisR.y)<=250)
            {
                int se=1;
                if (y<thisR.y)se=-1;
                control[id].left=-150*se;
                control[id].right=150*se;
                control[id].left_rotation=32;
                control[id].right_rotation=32;
            }
            else if((1400-x)*(1400-x)+(y-350)*(y-350)>22500)
            {
                target.y=0;
                target.x=pozitie.x=1400+(x-1400)/2;
                pozitie.y=350+(y-350)/2;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }

            else
            {
                target.y=700;
                target.x=pozitie.x=1150;
                pozitie.y=350;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
        }
        else
        {
            target.y=700;
            target.x=pozitie.x=250;
            pozitie.y=350;
            go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
        }
    }
}

void activedef (int id)
{
    int x=coords[0].x;
    int y=coords[0].y;
    RobotCoords target,pozitie;
    RobotCoords thisR=coords[id];
    if (B)
    {
        if (x<700)
        {
            if (x<100)
            {
                if(y<225)
                {
                    if(thisR.x>=150)
                    {
                        target.y=y;
                        pozitie.y=175;
                        target.x=x;
                        pozitie.x=90;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=y;
                        pozitie.y=175;
                        target.x=x;
                        pozitie.x=20;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else if (y>375)
                {
                    if(thisR.x>=150)
                    {
                        target.y=y;
                        pozitie.y=425;
                        target.x=x;
                        pozitie.x=90;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=y;
                        pozitie.y=425;
                        target.x=x;
                        pozitie.x=20;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else
                {
                    target.y=350;
                    target.x=700;
                    pozitie.x=500;
                    pozitie.y=350;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(y>600)
            {
                if(thisR.y<550)
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=610;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
                else
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=680;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(y<100)
            {
                if(thisR.y<550)
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=90;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
                else
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=20;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if (care_poarta(id))
            {
                target.y=y;
                target.x=pozitie.x=x;
                pozitie.y=y;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else
            {
                target.y=700;
                target.x=pozitie.x=500;
                pozitie.y=350;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }

        }
        else
        {
            target.y=700;
            target.x=pozitie.x=250;
            pozitie.y=350;
            go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
        }

    }
    else
    {
        if(x>700)
        {
            if (x>1300)
            {
                if(y<225)
                {
                    if(thisR.x<=1250)
                    {
                        target.y=y;
                        pozitie.y=175;
                        target.x=x;
                        pozitie.x=1310;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=y;
                        pozitie.y=175;
                        target.x=x;
                        pozitie.x=1380;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else if (y>375)
                {
                    if(thisR.x<=1250)
                    {
                        target.y=y;
                        pozitie.y=425;
                        target.x=x;
                        pozitie.x=1310;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                    else
                    {
                        target.y=y;
                        pozitie.y=425;
                        target.x=x;
                        pozitie.x=1380;
                        go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                    }
                }
                else
                {
                    target.y=700;
                    target.x=pozitie.x=900;
                    pozitie.y=350;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(y>600)
            {
                if(thisR.y<550)
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=610;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
                else
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=680;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(y<100)
            {
                if(thisR.y<550)
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=90;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
                else
                {
                    target.y=y;
                    target.x=pozitie.x=x;
                    pozitie.y=20;
                    go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
                }
            }
            else if(care_poarta(id))
            {
                target.y=y;
                target.x=pozitie.x=x;
                pozitie.y=y;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
            else
            {
                target.y=700;
                target.x=pozitie.x=900;
                pozitie.y=350;
                go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
            }
        }
        else
        {
            target.y=700;
            target.x=pozitie.x=250;
            pozitie.y=350;
            go_to_pozitie_cu_spatele2(thisR,target,pozitie,id);
        }
    }
}
