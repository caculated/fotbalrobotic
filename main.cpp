#include "structures.h"
#include "declarations.h"
#include "definitions.h"
#include "functions.h"

#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <iomanip>
#include <iostream>
#include <mosquitto.h>
#include <pthread.h>
using namespace std;
static int run = 1;

int main()
{
    int rc=0;
    int mid;
    int *p;

    if(B==1)
    {
        zona1_x1 = 350; tinta_x = susx;
        zona1_x2 = 700;

        zona2_x1 = 0;
        zona2_x2 = 350;
    }
    else
    {
        zona1_x1 = 700; tinta_x = josx;
        zona1_x2 = 1050;

        zona2_x1 = 1050;
        zona2_x2 = 1400;
    }

    pTYSON.x = zona2_x1+50; pTYSON.y = 350;
    pPUTIN.x = zona1_x1+50; pPUTIN.y = 350;

    mosquitto_lib_init();
    mosq = mosquitto_new(clientid, true, NULL);
    
    if(mosq){
         
        mosquitto_connect_callback_set(mosq, connect_callback);
        mosquitto_message_callback_set(mosq, message_callback);
        
        rc = mosquitto_connect(mosq, mqtt_host, mqtt_port, 60);
        mosquitto_subscribe(mosq, NULL, "coords", 0);   
        
//        t1.idt = pthread_create(&(t1.thread), NULL, get_publish, (void *) p); 
        //init_portar();
        gettimeofday(&tv, NULL);
        rc = mosquitto_loop_start(mosq);
        while(run)
        {

           if(need_to_send())
           {
                k++;
                do_robot_control_loop();
                if (k>27720) k%=27721;
            }

            if(run && rc){
                sleep(1);
                mosquitto_reconnect(mosq);
            }
        }

        mosquitto_destroy(mosq);
    }

    mosquitto_lib_cleanup();
    pthread_join(t1.thread, NULL);
    
   
    return rc;
}
