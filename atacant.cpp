#include "structures.h"
#include "functions.h"
#include "declarations.h"
#include <cmath>
#include <unistd.h>
#include <iostream>
#include "structures.h"
using namespace std;
int timelapse = 1;

void position( int robo, double x, double y )
{
    int desired_angle = 0, theta_e = 0, d_angle = 0, vl,vr, vc = 70;
    double dx, dy, d_e, Ka = 10.0/90.0;
    int robox = coords[robo].x, roboy = coords[robo].y, robo_angle = coords[robo].angle;

    dx = x - robox;
    dy = -y + roboy;
    d_e = sqrt(dx * dx + dy * dy);

    if (dx == 0 && dy == 0)desired_angle = 90;
    else desired_angle = (int)(180. / PI * atan2((double)(dy), (double)(dx)));
    theta_e = desired_angle - robo_angle;

    while (theta_e > 180) theta_e -= 360;
    while (theta_e < -180) theta_e += 360;

    if (d_e > 100.)Ka = 17. / 90.;
        else if (d_e > 50)Ka = 19. / 90.;
            else if (d_e > 30)Ka = 21. / 90.;
                else if (d_e > 20)Ka = 23. / 90.;
                    else Ka = 25. / 90.;

    if (theta_e > 95 || theta_e < -95)
    {
        theta_e += 180;

        if (theta_e > 180) theta_e -= 360;
        if (theta_e > 80) theta_e = 80;
        if (theta_e < -80) theta_e = -80;
        if (d_e < 5.0 && abs(theta_e) < 40) Ka = 0.1;

        vr = (int)(-vc * (1.0 / (1.0 + exp(-3.0 * d_e)) - 0.3) + Ka * theta_e);
        vl = (int)(-vc * (1.0 / (1.0 + exp(-3.0 *d_e)) - 0.3)- Ka * theta_e);
    }
        else if (theta_e < 85 && theta_e > -85)
            {
                if (d_e < 5.0 && abs(theta_e) < 40) Ka = 0.1;
                vr = (int)( vc * (1.0 / (1.0 + exp(-3.0 * d_e)) - 0.3) + Ka * theta_e);
                vl = (int)( vc * (1.0 / (1.0 + exp(-3.0 * d_e)) - 0.3) - Ka * theta_e);
            }
            else
            {
                vr = (int)(+.17 * theta_e); vl = (int)(-.17 * theta_e);
            }

    control[robo].left = vl*1.85;
    control[robo].right = vr*1.85;
    control[robo].left_rotation = 35;
    control[robo].right_rotation = 35;
    control[robo].time = 0;

}
void take_coordinates(int robot_id)
{
    cout<<coords[robot_id].angle<<endl<<coords[robot_id].x<<endl<<coords[robot_id].y;
}
void ataca_mingea2(int id_robot)
{
    double mingex = coords[0].x, mingey = coords[0].y;
    double robot_unghi = coords[id_robot].angle;
    int s,s2;
    int v;
    int x=coords[id_robot].x,y=coords[id_robot].y;

        int unghi_atac = unghiuldorit(id_robot, mingex,mingey);

        int ar=unghi_atac-coords[id_robot].angle;
        if (ar<0) ar += 360;

        if (k%3)
        {
            if(ar>180) semne[id_robot]=-1;
            else semne[id_robot]=1;
            if(ar>90&&ar<270) semne2[id_robot]=-1;
            else semne2[id_robot]=1;
        }
        s = semne[id_robot];
        s2= semne2[id_robot];

        if (s2==-1)unghi_atac+=180;


        int limita_plus = unghi_atac + 15;
        if (limita_plus>=360) limita_plus%=360;
        int limita_minus = unghi_atac - 15;
        if (limita_minus<0)
        {
            limita_minus=-limita_minus;
            limita_minus%=360;
            limita_minus=360-limita_minus;
        }
        if (limita_plus>limita_minus)
        {
            if(robot_unghi > limita_plus || robot_unghi < limita_minus)
            {
                if(k1[id_robot]==0)
                {
                    int limita_plus2 = unghi_atac + 30;
                    if (limita_plus2>=360) limita_plus2%=360;
                    int limita_minus2 = unghi_atac - 30;
                    if (limita_minus2<0)
                    {
                        limita_minus2=-limita_minus2;
                        limita_minus2%=360;
                        limita_minus2=360-limita_minus2;
                    }
                    int limita_plus3 = unghi_atac + 60;
                    if (limita_plus3>=360) limita_plus3%=360;
                    int limita_minus3 = unghi_atac - 60;
                    if (limita_minus3<0)
                    {
                        limita_minus3=-limita_minus3;
                        limita_minus3%=360;
                        limita_minus3=360-limita_minus3;
                    }


                    control[id_robot].left = -40*s*s2;
                    control[id_robot].right = 40*s*s2;
                    if (robot_unghi > limita_plus3 || robot_unghi < limita_minus3)
                    {
                        control[id_robot].left_rotation = 3;
                        control[id_robot].right_rotation = 3;
                    }
                    else if(robot_unghi > limita_plus2 || robot_unghi < limita_minus2)
                    {
                        control[id_robot].left_rotation = 2;
                        control[id_robot].right_rotation = 2;
                    }
                    else
                    {
                        control[id_robot].left_rotation = 1;
                        control[id_robot].right_rotation = 1;
                    }

                }




                k1[id_robot]++;
                k2[id_robot]=0;
                if (k1[id_robot]>=8)k1[id_robot]=0;
            }
            else
            {
                cout<<"in else"<<endl;
                if (k2[id_robot]<11) k2[id_robot]++;
                v=k2[id_robot]*10;
                control[id_robot].left = (50+v)*s2;
                control[id_robot].right = (50+v)*s2;
                control[id_robot].left_rotation = 16;
                control[id_robot].right_rotation = 16;
                k1[id_robot]=0;

            }

        }
        else
        {
            if(robot_unghi > limita_plus && robot_unghi < limita_minus)
            {
                if(k1[id_robot]==0)
                {
                    int limita_plus2 = unghi_atac + 30;
                    if (limita_plus2>=360) limita_plus2%=360;
                    int limita_minus2 = unghi_atac - 30;
                    if (limita_minus2<0)
                    {
                        limita_minus2=-limita_minus2;
                        limita_minus2%=360;
                        limita_minus2=360-limita_minus2;
                    }
                    int limita_plus3 = unghi_atac + 60;
                    if (limita_plus3>=360) limita_plus3%=360;
                    int limita_minus3 = unghi_atac - 60;
                    if (limita_minus3<0)
                    {
                        limita_minus3=-limita_minus3;
                        limita_minus3%=360;
                        limita_minus3=360-limita_minus3;
                    }


                    control[id_robot].left = -40*s*s2;
                    control[id_robot].right = 40*s*s2;
                    if (robot_unghi > limita_plus3 && robot_unghi < limita_minus3)
                    {
                        control[id_robot].left_rotation = 3;
                        control[id_robot].right_rotation = 3;
                    }
                    else if(robot_unghi > limita_plus2 && robot_unghi < limita_minus2)
                    {
                        control[id_robot].left_rotation = 2;
                        control[id_robot].right_rotation = 2;
                    }
                    else
                    {
                        control[id_robot].left_rotation = 1;
                        control[id_robot].right_rotation = 1;
                    }

                }




                k1[id_robot]++;
                k2[id_robot]=0;
                if (k1[id_robot]>=8)k1[id_robot]=0;
            }
            else
            {
                cout<<"in else"<<endl;
                if (k2[id_robot]<11) k2[id_robot]++;
                v=k2[id_robot]*10;
                control[id_robot].left = (50+v)*s2;
                control[id_robot].right = (50+v)*s2;
                control[id_robot].left_rotation = 16;
                control[id_robot].right_rotation = 16;
                k1[id_robot]=0;

            }

        }





        //cerr<<(robot_unghi <= limita_plus && robot_unghi >= limita_minus)<<endl;

}
void retragere(int id_robot,int id_robot2) // doar atacanti
{
    double x1,y1;
    if (B) x1 = 250;
    else x1 = 1150;
    if(coords[id_robot].y < coords[id_robot2].y) y1 = 200;
    else y1 = 600;
    double robot_unghi = coords[id_robot].angle;
    int s,s2;
    int v;
    int x=coords[id_robot].x,y=coords[id_robot].y;

        int unghi_atac = unghiuldorit(id_robot,x1,y1);

        int ar=unghi_atac-coords[id_robot].angle;
        if (ar<0) ar += 360;

        if (k%3)
        {
            if(ar>180) semne[id_robot]=-1;
            else semne[id_robot]=1;
            if(ar>90&&ar<270) semne2[id_robot]=-1;
            else semne2[id_robot]=1;
        }
        s = semne[id_robot];
        s2= semne2[id_robot];

        if (s2==-1)unghi_atac+=180;


        int limita_plus = unghi_atac + 15;
        if (limita_plus>=360) limita_plus%=360;
        int limita_minus = unghi_atac - 15;
        if (limita_minus<0)
        {
            limita_minus=-limita_minus;
            limita_minus%=360;
            limita_minus=360-limita_minus;
        }


        if (limita_plus>limita_minus)
        {
            if(robot_unghi > limita_plus || robot_unghi < limita_minus)
            {
                if(k1[id_robot]==0)
                {
                    int limita_plus2 = unghi_atac + 30;
                    if (limita_plus2>=360) limita_plus2%=360;
                    int limita_minus2 = unghi_atac - 30;
                    if (limita_minus2<0)
                    {
                        limita_minus2=-limita_minus2;
                        limita_minus2%=360;
                        limita_minus2=360-limita_minus2;
                    }
                    int limita_plus3 = unghi_atac + 60;
                    if (limita_plus3>=360) limita_plus3%=360;
                    int limita_minus3 = unghi_atac - 60;
                    if (limita_minus3<0)
                    {
                        limita_minus3=-limita_minus3;
                        limita_minus3%=360;
                        limita_minus3=360-limita_minus3;
                    }


                    control[id_robot].left = -40*s*s2;
                    control[id_robot].right = 40*s*s2;
                    if (robot_unghi > limita_plus3 || robot_unghi < limita_minus3)
                    {
                        control[id_robot].left_rotation = 3;
                        control[id_robot].right_rotation = 3;
                    }
                    else if(robot_unghi > limita_plus2 || robot_unghi < limita_minus2)
                    {
                        control[id_robot].left_rotation = 2;
                        control[id_robot].right_rotation = 2;
                    }
                    else
                    {
                        control[id_robot].left_rotation = 1;
                        control[id_robot].right_rotation = 1;
                    }

                }




                k1[id_robot]++;
                k2[id_robot]=0;
                if (k1[id_robot]>=8)k1[id_robot]=0;
            }
            else
            {
                cout<<"in else"<<endl;
                if (k2[id_robot]<11) k2[id_robot]++;
                v=k2[id_robot]*10;
                control[id_robot].left = (50+v)*s2;
                control[id_robot].right = (50+v)*s2;
                control[id_robot].left_rotation = 16;
                control[id_robot].right_rotation = 16;
                k1[id_robot]=0;

            }

        }
        else
        {
            if(robot_unghi > limita_plus && robot_unghi < limita_minus)
            {
                if(k1[id_robot]==0)
                {
                    int limita_plus2 = unghi_atac + 30;
                    if (limita_plus2>=360) limita_plus2%=360;
                    int limita_minus2 = unghi_atac - 30;
                    if (limita_minus2<0)
                    {
                        limita_minus2=-limita_minus2;
                        limita_minus2%=360;
                        limita_minus2=360-limita_minus2;
                    }
                    int limita_plus3 = unghi_atac + 60;
                    if (limita_plus3>=360) limita_plus3%=360;
                    int limita_minus3 = unghi_atac - 60;
                    if (limita_minus3<0)
                    {
                        limita_minus3=-limita_minus3;
                        limita_minus3%=360;
                        limita_minus3=360-limita_minus3;
                    }


                    control[id_robot].left = -40*s*s2;
                    control[id_robot].right = 40*s*s2;
                    if (robot_unghi > limita_plus3 && robot_unghi < limita_minus3)
                    {
                        control[id_robot].left_rotation = 3;
                        control[id_robot].right_rotation = 3;
                    }
                    else if(robot_unghi > limita_plus2 && robot_unghi < limita_minus2)
                    {
                        control[id_robot].left_rotation = 2;
                        control[id_robot].right_rotation = 2;
                    }
                    else
                    {
                        control[id_robot].left_rotation = 1;
                        control[id_robot].right_rotation = 1;
                    }

                }




                k1[id_robot]++;
                k2[id_robot]=0;
                if (k1[id_robot]>=8)k1[id_robot]=0;
            }
            else
            {
                cout<<"in else"<<endl;
                if (k2[id_robot]<11) k2[id_robot]++;
                v=k2[id_robot]*10;
                control[id_robot].left = (50+v)*s2;
                control[id_robot].right = (50+v)*s2;
                control[id_robot].left_rotation = 16;
                control[id_robot].right_rotation = 16;
                k1[id_robot]=0;

            }

        }



        //cerr<<(robot_unghi <= limita_plus && robot_unghi >= limita_minus)<<endl;

}
void rotire_spre_minge_si_pornire_spre_minge ()
{
    control[rId].right = 30;
    control[rId].left = -30;
    control[rId].time = 0;
    control[rId].right_rotation = 32000;
    control[rId].left_rotation  = 32000;

    request_publish(rId);

    ///while(atan2(coords[rId].y - coords[0].y,coords[rId].x - coords[0].x)-10>coords[rId].angle  &&  coords[rId].angle < atan2(coords[rId].y - coords[0].y,coords[rId].x - coords[0].x)+10);
    while(70 < coords[rId].angle && coords[rId].angle > 110);


    control[rId].right = 60;
    control[rId].left = 60;
    control[rId].time = 0;
    control[rId].right_rotation = 32000;
    control[rId].left_rotation  = 32000;

    request_publish(rId);
}
void strategie_pentru_crimeea (int id)
{
    int daca;
    int a = unghiuldorit(id,coords[0].x,coords[0].y);
    point p;
    if(B)
    {
        if(135>a&&a<=225)
            daca=2;
        else
            if(45>a&&a<=135)
                daca=1;
            else
                if(225>a&&a<=315)
                    daca=1;
                else
                    daca=0;
        switch(daca)
        {
            case 2:
                if(dreapta(coords[id].x,coords[id].y,coords[0].x,coords[0].y+100)<dreapta(coords[id].x,coords[id].y,coords[0].x,coords[0].y-100))
                    p={coords[0].x-65,coords[0].y+100};
                else
                    p={coords[0].x-65,coords[0].y-100};
                break;
            case 1:
                p={coords[0].x-100,coords[0].y}; break;
        }
        if(daca)
            go_to(id,p);
        else
            ataca_mingea2(id);

    }
    else
{
    if(135>a&&a<=225)
        daca=0;
    else
        if(45>a&&a<=135)
            daca=1;
        else
            if(225>a&&a<=315)
                daca=1;
            else
                daca=2;
    switch(daca)
    {
        case 2:
            if(dreapta(coords[id].x,coords[id].y,coords[0].x,coords[0].y+100)<dreapta(coords[id].x,coords[id].y,coords[0].x,coords[0].y-100))
                p={coords[0].x+65,coords[0].y+100};
            else
                p={coords[0].x+65,coords[0].y-100};
            break;
        case 1:
            p={coords[0].x+100,coords[0].y}; break;
    }
    if(daca)
        go_to(id,p);
    else
        ataca_mingea2(id);

}
}
void ataca(int robo)
{
    RobotCoords coordonate_poarta1, coordonate_poarta2, poarta, target, target_aux;
    coordonate_poarta1.x = tinta_x;
    coordonate_poarta2.x = tinta_x;
    coordonate_poarta1.y = tinta_dreaptay;
    coordonate_poarta2.y = tinta_stangay;
    control[robo].time = 0;

    poarta = points_on_the_poarta(coordonate_poarta1, coordonate_poarta2, robo);

    target = point_on_the_line(poarta, coords[0], 2*diametrub);

    target_aux = target;
    target = calculate_target(target,0,24);
    if(target.x != target_aux.x || target.y != target_aux.y) { poarta = coords[0]; }
    if(test_if_within_circle(coords[robo],target,diametrub))target = poarta; //sau coords[0].

    position(robo, target.x, target.y);

}
